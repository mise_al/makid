package com.excelreader;

import com.excelreader.ui.GoogleHomePage;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

public class ExcelReaderTest {

    private static WebDriver driver;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/mac_chromedriver/chromedriver");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void readFromExcelSpreadsheet() {


        try {
            File excelFile = new File("src/main/resources/ExcelFiles/Vehicles.xlsx"); //set location of file. only use new .xlsx file

            FileInputStream fileInputStream = new FileInputStream(excelFile);

            XSSFWorkbook excelWorkBook = new XSSFWorkbook(fileInputStream); //locate workbook

            XSSFSheet excelWorkSheet1 = excelWorkBook.getSheetAt(0); //locate first worksheet

            int lastRowNumber = excelWorkSheet1.getLastRowNum(); //get count of rows
            int lastColumNumber = excelWorkSheet1.getRow(0).getPhysicalNumberOfCells(); //get count of columns of row 1

            ArrayList<ArrayList<String>> excelDataArrayList = new ArrayList<ArrayList<String>>(++lastRowNumber); //create an array or arrays

            /*
            * Iterate through the array an add each item into array list which will be used to search the google page
             */
            for (int i = 0; i < lastRowNumber; i++) {
                ArrayList<String> rowList = new ArrayList<String>();
                for (int k = 0; k < lastColumNumber; k++) {
                    rowList.add(excelWorkSheet1.getRow(i).getCell(k).toString());
                }
                excelDataArrayList.add(rowList);
            }

            // clean up
            excelWorkBook.close();
            fileInputStream.close();

            // Use array list as data list to search in google
            for (ArrayList arrayList : excelDataArrayList) {
                for (int i = 0; i < arrayList.size(); i++) {
                    StringBuilder searchString = new StringBuilder(arrayList.get(i).toString());
                    driver.navigate().to("https://www.google.com/");
                    GoogleHomePage.searchForText(driver, searchString.toString());

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }


    }

}
