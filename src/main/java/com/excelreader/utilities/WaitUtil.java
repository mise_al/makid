package com.excelreader.utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;
import java.util.NoSuchElementException;

public class WaitUtil {
    private static Duration defaultWaitTime = Duration.ofSeconds(30);
    private static Duration pollingIntervalTime = Duration.ofSeconds(1);

    public static void waitForElementToBeVisible(WebDriver driver, By elementLocator) {
        new FluentWait<>(driver).withTimeout(defaultWaitTime).pollingEvery(pollingIntervalTime)
                .ignoring(NoSuchElementException.class).
                until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
    }

    public static void waitForElementToBeClickable(WebDriver driver, By elementLocator) {
        new FluentWait<>(driver).withTimeout(defaultWaitTime).pollingEvery(pollingIntervalTime)
                .ignoring(NoSuchElementException.class).
                until(ExpectedConditions.elementToBeClickable(elementLocator));
    }

    public static void waitForPageToLoad(WebDriver driver) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        if (executor.executeScript("return document.readyState").equals("complete")) {
            return;
        }
        else {
            waitForPageToLoad(driver);
        }
    }

    public static void sleep(int sleepDuration) {
        try {
            Thread.sleep(sleepDuration);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
