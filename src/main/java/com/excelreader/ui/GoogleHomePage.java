package com.excelreader.ui;

import com.excelreader.utilities.WaitUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class GoogleHomePage {

    private static By searchField = By.cssSelector("input[title='Search']");


    public static void searchForText(WebDriver driver, String inputText) {
        WaitUtil.waitForPageToLoad(driver);
        WaitUtil.waitForElementToBeClickable(driver, searchField);
        PageUtil.clearTextField(driver, searchField);
        PageUtil.sendKeys(driver, searchField, inputText);
        PageUtil.sendEnterKey(driver, searchField);
        WaitUtil.waitForPageToLoad(driver);
    }
}
