package com.excelreader.ui;

import org.apache.xmlbeans.impl.xb.xsdschema.KeyDocument;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class PageUtil {

    public static void clearTextField(WebDriver driver, By elementLocator) {
        driver.findElement(elementLocator).clear();
    }

    public static void sendKeys(WebDriver driver, By elementLocator, String inputText) {
        driver.findElement(elementLocator).sendKeys(inputText);
    }

    public static void sendEnterKey(WebDriver driver, By elementLocator) {
        driver.findElement(elementLocator).sendKeys(Keys.ENTER);
    }
}
